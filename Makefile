
#PROJECT = nano
export TOP = DE1_SoC
# Répertoire courant
export TOP_DIR = $(shell pwd)

# répertoire des fichiers source
vpath %.sv $(TOP_DIR)/src

MAKE_CMD = make
.PHONY : clean all program syn

# règle par défaut
all: syn

syn:
	$(MAKE_CMD) -C syn syn

program:
	$(MAKE_CMD) -C syn program

#simu:
#	$(MAKE_CMD) -C simulation "FILES = $(COMMON_FILES) $(PC_FILE) $(CTR_FILE) $(ALU_FILE) disass.sv simu.sv"

clean :
	$(MAKE_CMD) -C simulation clean
	$(MAKE_CMD) -C syn clean
