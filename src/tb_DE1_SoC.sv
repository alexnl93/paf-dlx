module tb_DE1_SoC();
   
   logic clk;
   logic [3:0] key;
   logic [6:0] hex0;
   logic [6:0] hex1;
   logic [6:0] hex2;
   logic [6:0] hex3;
   logic [6:0] hex4;
   logic [6:0] hex5;
   logic [9:0] ledr;
   logic [9:0] sw;   
   
   DE1_SoC   DE1_SoC1(.clock_50(clk),
		      .hex0(hex0),.hex1(hex1),.hex2(hex2),.hex3(hex3),.hex4(hex4),.hex5(hex5),
		      .key(key),
		      .ledr(ledr),
		      .sw(sw));
   
   always
     begin
	#10ns
	  clk <= ~clk;
     end

   
   initial
     begin
	clk <= 0;
	key <= 4'b0000;
	#50ns

	  key <= 4'b1111;

     end

endmodule // tb_DEreg_SoC