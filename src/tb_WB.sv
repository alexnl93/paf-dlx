module tb_WB();

   logic [31:0] sd;
   logic [31:0] d_data_read;
   logic [31:0] pc;
   logic [5:0] 	opcode;
   logic [31:0] ved;

   WB wb1(.sd(sd),.d_data_read(d_data_read),.pc(pc),.opcode(opcode),.ved(ved));

   initial
     begin
	sd<=32'b11110_11110_11110_11110_11110_11110_10;
	d_data_read<=32'b00001_00001_00001_00001_00001_00001_01;
	pc<=0;
                opcode<=0;

	#10ns

	  opcode<=6'h03;

	#10ns

	  opcode<=6'h13;
	pc<=32'd35;
	

	#10ns

	  opcode<=6'h13;
	pc<=32'd152;
	

	#10ns

	  opcode<=6'h23;

	#10ns

	  opcode<=6'h13;

	#10ns

	  opcode<=6'h1;


	

end
endmodule
 
   