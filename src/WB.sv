module WB(
	  //Entrees
	  input logic [31:0]  sd,
	  input logic [31:0]  d_data_read,
	  input logic [31:0]  PC,
	  input logic [5:0] opcode,
	  //Sorties
	  output logic [31:0] ved
	  );


   always@(*) //choix de la valeur vd (ici VED) que l'on va ecrire dans le registre Rd quand WE est à 0
     
     case(opcode)
       6'h03:ved <= PC+4;
       6'h13:ved <= PC+4;
       6'h23:ved <= d_data_read;
       default:ved <= sd;
       
     endcase // case (opcode)


endmodule // WB

   
   
   
   
   