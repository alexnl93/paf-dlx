   module EX (
	      //Entrees
	      input logic [25:0]  value,
	      input logic [15:0]  immediate, 
	      input logic [31:0]  PC,
	      input logic [31:0]  Rs1,
	      input logic [31:0]  Rs2,
	      input logic [5:0]   opcode,
	      input logic [1:0]   format,
	      input logic 	  load_e1, load_e2,
	      input logic ved,
	      //Sorties
	      output logic [31:0] Sd,
	     
	      input logic [1:0]   format, 
	      //Sorties
	      output logic [31:0] Sd
	      );

 
   logic [31:0] 	 imm_ext;
   logic [31:0] 	 val_ext;
   logic signed [31:0]   imm_sign_ext;
   logic signed [31:0]   val_sign_ext;
   

   always @(*) //calcul de extend immediate  et sign extend value
     begin
	imm_ext <= {16'b0, immediate};
	if (immediate[15])
	  imm_sign_ext <= {16'hffff, immediate};
	else
	  imm_sign_ext <= {16'b0, immediate};
	
     end

   always@(*) //calcul de extend value
		   begin
		      val_ext <= {16'b0, value};			
		   end
   
   
   always@(*) //cas du meme opcode pour 2 format différents
				 begin
				    if (opcode == 6'h04)
				      begin
					 if (format == 1) Sd <= PC + (Rs1 == 0 ? imm_sign_ext : 0);
					 else Sd <= Rs1 << (Rs2 % 32);       
				      end
				    end
  always@(*) //calcul de l'alu en fonction de l'opcode
	
     case(opcode)

       6'h20:	Sd <= Rs1 + Rs2;
       6'h08:	Sd <= Rs1 + imm_sign_ext;      
       6'h24:	Sd <= Rs1 & Rs2;       
       6'h0c:	Sd <= Rs1 & imm_ext;       
       //	  6'h04: if(format==1) Sd <= PC + (Rs1 == 0 ? imm_sign_ext : 0);  
       6'h05:	Sd <= PC + (Rs1 != 0 ? imm_sign_ext : 0);       
       6'h02:	Sd <= PC + val_ext;       
       6'h03:	Sd <= PC + val_ext;       
       6'h13:	Sd <= Rs1 ;      
       6'h12:	Sd <= Rs1;       
       6'h0f:	Sd <= imm_ext << 16;       
       6'h23:	Sd = Rs1 + imm_sign_ext;       
       6'h25:	Sd <= Rs1 | Rs2;      
       6'h0d:	Sd <= Rs1 | imm_ext;       
       6'h28:	Sd <= (Rs1 == Rs2 ? 1 : 0);       
       6'h18:	Sd <= (Rs1 == imm_sign_ext ? 1 : 0);       
       6'h2c:	Sd <= (Rs1 <= Rs2 ? 1 : 0);       
       6'h1c:	Sd <= (Rs1 <= imm_sign_ext ? 1 : 0);       
       //	  6'h04:if (format==0)	Sd <= Rs1 << (Rs2 % 32);       
       6'h14:	Sd <= Rs1 << (imm_ext % 32);       
       6'h2a:	Sd <= (Rs1 < Rs2 ? 1 : 0);      
       6'h1a:	Sd <= (Rs1 < imm_sign_ext ? 1 : 0);       
       6'h29:	Sd <= (Rs1 != Rs2 ? 1 : 0);       
       6'h19:	Sd <= (Rs1 != imm_sign_ext ? 1 : 0);     
       6'h07:	Sd <= Rs1 >>> (Rs2 % 32);      
       6'h17:	Sd <= Rs1 >>> (imm_ext % 32);       
       6'h06:	Sd <= Rs1 >> (Rs2 % 32);       
       6'h16:	Sd <= Rs1 >> (imm_ext % 32);       
       6'h22:	Sd <= Rs1 - Rs2;       
       6'h0a:	Sd <= Rs1 - imm_sign_ext;      
       6'h2b:	Sd <= Rs1 + imm_sign_ext;
       6'h26:	Sd <= Rs1 ^ Rs2;
       6'h0e:	Sd <= Rs1 ^ imm_ext;

       

     endcase // case (opcode)

   always@(*)
		      
    if(load_e1)
			
      Rs1<=ed;
   
   if(load_e2)
     
      Rs2<=ed;

   if(load_2_e1)

     Rs1<=ved;

   if(load_2_e2)

     Rs2<=ved;
   
   
   
endmodule
