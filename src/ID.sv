module ID ( 
	    // Entrees
	    input logic [31:0] 	i_data_read,
	    input logic 	load_previous,
	    input logic 	clk,
	    //Sorties
	    output logic [5:0] 	opcode,
	    output logic [4:0] 	e1,e2,ed,
	    output logic [15:0] immediate,
	    output logic [25:0] value,
	    output logic [1:0] 	form,
	    output logic 	load_e1,load_e2,load_2_e1,load_2_e2
	    );
   logic [31:0] 		previous_ed,pprevious_ed;
   
   enum 			logic [1:0] 			{R,I,J} format;
   

   always @(*) // Generation du OpCode

     begin
	
	if (i_data_read[31:26]==6'b000000)
	  opcode<=i_data_read[5:0];
	else
	  opcode<=i_data_read[31:26];
     end // always @ (*)
   
   always@(*) // Generation du format

   begin

      if (i_data_read[31:26]==6'b000000)

	begin format<=R; form <=0;end

      else if (i_data_read[31:26]==6'b000010||i_data_read[31:26]==6'b000011)

	begin format<=J; form <=2;end

      else

	begin format<=I; form <=1;end
      
   end

   always@(*) // Generation des adresses de registres, de immediate et de value

			     begin

				// e1,e2 numero des registres en entree du BDR

				
				e1 <= i_data_read[25:21];
				e2 <= i_data_read[20:16];
				value<=i_data_read[25:0];
				immediate<=i_data_read[15:0];
				

				// Generation de ed, immediate, value
				
				if (format==R)
				   
				  ed <= i_data_read[15:11];
				

				else if(format==J)
				   

				  ed<=5'd31;
				
				
				
				else if ((format==I) && (opcode==6'h13))
				   
				  ed<=5'd31;
				
				else
				   
				  ed<=i_data_read[20:16];
				
				

			     end // always@ begin

   always@(*) // Voir si il va y avoir conflit
      
     if (previous_ed==e1||pprevious_ed==e1)

       load_e1<=1;
   
     else  if (previous_ed==e2||pprevious_ed==e2)
	
       load_e2<=1;

     else if(pprevious_ed==e2)

       load_2_e2<=1;

     else if(pprevious_ed==e1)

       load_2_e1<=1;
   
     else
	
       load_e1<=0;
   load_e2<=0;
   load_2_e1<=0;
   load_2_e2<=0;
   
   
   always@(posedge clk)

     previous_ed<=ed;
   
   pprevious_ed<=previous_ed;
   

endmodule // ID









