module   IF (
	  //Entrees
	     input logic [31:0]  sd, /*sortie de EX */
	     input logic [5:0] 	 opcode,
	     input logic 	 clk,
	     input logic 	 reset_n,
	     input logic 	 load_pc,
	     input logic [1:0] form,
	  //Sorties
	     output logic [31:0] pc
	  );


   
   always@(posedge clk or negedge reset_n)

     if (~reset_n)

       pc<=0;

     else

     if(~load_pc)

	 pc<=pc;

       else 
	  
	 case(opcode)
	   6'h04: if (form ==1)	pc <= sd; else pc <= pc + 4 ;       
	   6'h05:	pc <= sd    ;    
	   6'h02:	pc <= sd    ;   
	   6'h03:	pc <= sd    ;       
	   6'h13:	pc <= sd    ;   
	   6'h12:	pc <= sd    ;
	   default: pc <= pc + 4;

	 endcase // case (opcode)
endmodule // IF

