module DLX
  (
   input logic 	       clk,
   input logic 	       reset_n,
   output logic        load_previous,

   // RAM contenant les données
   output logic [31:0] d_address,
   input logic [31:0]  d_data_read,
   output logic [31:0] d_data_write,
   output logic        d_write_enable,
   input logic 	       d_data_valid,

   // ROM contenant les instructions
   output logic [31:0] i_address,
   input logic [31:0]  i_data_read,
   input logic 	       i_data_valid
   );

   enum 	       logic [2:0] {IF,ID,EX,ME,WB} etat;
   
   logic [1:0] 	       format;
   logic [31:0]        sd;
   logic [5:0] 	       opcode;
   logic [31:0]        pc;
   logic [4:0] 	       e1,e2,ed;   
   logic [31:0]        ved;
   logic [25:0]        value;
   logic [15:0]        immediate;
   logic 	       WE; //authorisation d'écrire dans le registre
   logic 	       load_pc;
   logic [31:0]        R0,R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15,R16,R17,R18,R19,R20,R21,R22,R23,R24,R25,R26,R27,R28,R29,R30,R31;
   logic [31:0]        RS1,RS2;
   
   
   
   
   IF If(.sd(sd),
	 .opcode(opcode),
	 .clk(clk),
	 .reset_n(reset_n),
	 .load_pc(load_pc),
	 .form(format),
	 .pc(pc))  ;
   
   
   ID id(.i_data_read(i_data_read),
	 .opcode(opcode),
	 .e1(e1),.e2(e2),.ed(ed),
	 .immediate(immediate),
	 .value(value),
	 .form(format));
   

   EX ex(.value(value),
	 .immediate(immediate),
	 .PC(pc),
	 .Rs1(RS1),.Rs2(RS2),//faire correspondre les images de e1 et e2 
	 .opcode(opcode),
	 .format(format),
	 .Sd(sd));
   
   WB wb(.sd(sd),
	 .d_data_read(d_data_read),
	 .PC(pc),
	 .opcode(opcode),
	 .ved(ved));
  


   always@(*)
     case (opcode)
       6'h04: begin d_write_enable <= 0; if (format == 1) WE <= 0; else if (etat == WB) WE <= 1; end       
       6'h05: begin WE <= 0; d_write_enable <= 0; end
       6'h02: begin WE <= 0; d_write_enable <= 0; end
       6'h12: begin WE <= 0; d_write_enable <= 0; end 
       6'h2b: begin d_write_enable <= 1; WE <= 0; end       	  
       default:  begin d_write_enable <= 0; if (etat == WB) WE <= 1;  end
     endcase


   always@(posedge clk)
     case (ed)
       5'd1: begin d_data_write <= R1; if(WE == 1) R1 <= ved; end
       5'd2: begin d_data_write <= R2; if(WE == 1) R2 <= ved; end
       5'd3: begin d_data_write <= R3; if(WE == 1) R3 <= ved; end
       5'd4: begin d_data_write <= R4; if(WE == 1) R4 <= ved; end
       5'd5: begin d_data_write <= R5; if(WE == 1) R5 <= ved; end
       5'd6: begin d_data_write <= R6; if(WE == 1) R6 <= ved; end
       5'd7: begin d_data_write <= R7; if(WE == 1) R7 <= ved; end
       5'd8: begin d_data_write <= R8; if(WE == 1) R8 <= ved; end
       5'd9: begin d_data_write <= R9; if(WE == 1) R9 <= ved; end
       5'd10: begin d_data_write <= R10; if(WE == 1) R10 <= ved; end
       5'd11: begin d_data_write <= R11; if(WE == 1) R11 <= ved; end
       5'd12: begin d_data_write <= R12; if(WE == 1) R12 <= ved; end
       5'd13: begin d_data_write <= R13; if(WE == 1) R13 <= ved; end
       5'd14: begin d_data_write <= R14; if(WE == 1) R14 <= ved; end
       5'd15: begin d_data_write <= R15; if(WE == 1) R15 <= ved; end
       5'd16: begin d_data_write <= R16; if(WE == 1) R16 <= ved; end
       5'd17: begin d_data_write <= R17; if(WE == 1) R17 <= ved; end
       5'd18: begin d_data_write <= R18; if(WE == 1) R18 <= ved; end
       5'd19: begin d_data_write <= R19; if(WE == 1) R19 <= ved; end
       5'd20: begin d_data_write <= R20; if(WE == 1) R20 <= ved; end
       5'd21: begin d_data_write <= R21; if(WE == 1) R21 <= ved; end
       5'd22: begin d_data_write <= R22; if(WE == 1) R22 <= ved; end
       5'd23: begin d_data_write <= R23; if(WE == 1) R23 <= ved; end
       5'd24: begin d_data_write <= R24; if(WE == 1) R24 <= ved; end
       5'd25: begin d_data_write <= R25; if(WE == 1) R25 <= ved; end
       5'd26: begin d_data_write <= R26; if(WE == 1) R26 <= ved; end
       5'd27: begin d_data_write <= R27; if(WE == 1) R27 <= ved; end
       5'd28: begin d_data_write <= R28; if(WE == 1) R28 <= ved; end
       5'd29: begin d_data_write <= R29; if(WE == 1) R29 <= ved; end
       5'd30: begin d_data_write <= R30; if(WE == 1) R30 <= ved; end
       5'd31: begin d_data_write <= R31; if(WE == 1) R31 <= ved; end
     endcase // case (ed)
	 
    always@(*)	  
     case (e1)
      5'd0: RS1 <= 0;
      5'd1: RS1 <= R1; 
      5'd2: RS1 <= R2;
      5'd3: RS1 <= R3; 
      5'd4: RS1 <= R4;
      5'd5: RS1 <= R5; 
      5'd6: RS1 <= R6;
      5'd7: RS1 <= R7; 
      5'd8: RS1 <= R8;
      5'd9: RS1 <= R9; 
      5'd10: RS1 <= R10;
      5'd11: RS1 <= R11; 
      5'd12: RS1 <= R12;
      5'd13: RS1 <= R13; 
      5'd14: RS1 <= R14;
      5'd15: RS1 <= R15; 
      5'd16: RS1 <= R16;
      5'd17: RS1 <= R17; 
      5'd18: RS1 <= R18;
      5'd19: RS1 <= R19;
      5'd20: RS1 <= R20;
      5'd21: RS1 <= R21; 
      5'd22: RS1 <= R22;
      5'd23: RS1 <= R23; 
      5'd24: RS1 <= R24;
      5'd25: RS1 <= R25; 
      5'd26: RS1 <= R26;
      5'd27: RS1 <= R27; 
      5'd28: RS1 <= R28;
      5'd29: RS1 <= R29;
      5'd30: RS1 <= R30;
      5'd31: RS1 <= R31; 
     endcase


    always@(*)	  
     case (e2)
      5'd0: RS2 <= 0;
      5'd1: RS2 <= R1; 
      5'd2: RS2 <= R2;
      5'd3: RS2 <= R3; 
      5'd4: RS2 <= R4;
      5'd5: RS2 <= R5; 
      5'd6: RS2 <= R6;
      5'd7: RS2 <= R7; 
      5'd8: RS2 <= R8;
      5'd9: RS2 <= R9; 
      5'd10: RS2 <= R10;
      5'd11: RS2 <= R11; 
      5'd12: RS2 <= R12;
      5'd13: RS2 <= R13; 
      5'd14: RS2 <= R14;
      5'd15: RS2 <= R15; 
      5'd16: RS2 <= R16;
      5'd17: RS2 <= R17; 
      5'd18: RS2 <= R18;
      5'd19: RS2 <= R19;
      5'd20: RS2 <= R20;
      5'd21: RS2 <= R21; 
      5'd22: RS2 <= R22;
      5'd23: RS2 <= R23; 
      5'd24: RS2 <= R24;
      5'd25: RS2 <= R25; 
      5'd26: RS2 <= R26;
      5'd27: RS2 <= R27; 
      5'd28: RS2 <= R28;
      5'd29: RS2 <= R29;
      5'd30: RS2 <= R30;
      5'd31: RS2 <= R31; 
     endcase // case (e2)

   always@(*) if (etat == WB) load_pc <= 1; else load_pc <= 0;

   always@(*) i_address <= pc; 

   always@(*) if (etat == ME)  d_address <= sd;
   
   always@(posedge clk or negedge reset_n)
     if (~reset_n)
       begin
	  i_address <= 0;
	  etat <= IF;
       end
   
     else
      case(etat)  
	IF :   etat <= ID;
	ID : etat <= EX; load_previous<=1;
	EX : etat <= ME; load_previous<=0;	
	ME : etat <= WB;
	WB : begin etat <= IF; WE <= 0;end
	endcase
   
endmodule // DLX
