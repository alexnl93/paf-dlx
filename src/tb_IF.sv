module tb_IF ( ) ;

   logic [31:0] sd;
   logic [5:0] 	opcode;
   logic [31:0] pc;
   logic 	clk;
   logic 	reset_n;
   logic 	load_pc;
   
   
   
   

   IF if1(.sd(sd),.opcode(opcode),.pc(pc),.clk(clk),.reset_n(reset_n),.load_pc(load_pc));
   
   always

     begin
	#10ns
	  clk<=~clk;
     end


   
   initial

     begin

	sd<=32'b10100101010010101010101001001001;
	opcode<=6'b000000;
	pc<=32'b0;
	clk<=0;
	reset_n<=0;
	load_pc<=0;
	

	#10ns
	   
          load_pc<=1;
	reset_n<=1;
	opcode<=6'b111000;
	
	#20ns

	  opcode<=6'b111000;
	load_pc<=1;
	

	#20ns
	   
	  opcode<=6'b111100;
	load_pc<=0;
	

	#20ns

	  opcode<=6'b111110;
	load_pc<=1;
	

	#20ns

	  opcode<=6'b000110;

	#20ns

	  opcode<=6'b000100;
	sd<=32'b0000000101001010101010100100100;

	#20ns

	  opcode<=6'b111110;

     end // initial begin

endmodule // tb_IF
