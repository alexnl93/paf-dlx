module tb_EX();
   
   logic [25:0]  value;
   logic [15:0]  immediate; 
   logic [31:0]  i_data_read;
   logic [31:0]  pc;
   logic [31:0]  e1,e2;
   logic [5:0] 	 opcode;
   logic [1:0]  format;
   
      

     EX ex(.value(value),
	 .immediate(immediate),  
	 .PC(pc),
	 .Rs1(e1),.Rs2(e2),
	 .opcode(opcode),
	 .format(format),  
	 .Sd(sd));

 

   initial
     begin
	
	//numéro 1	
	opcode <= 6'h20;
	e1 <= 3;
	e2 <= 2;

	//numéro 2	
	#20ns
	opcode <= 6'h08;
	e1 <= 3;
	immediate <= -2;

	//numéro 3	
	#10ns
	opcode <= 6'h24;
	e1 <= 3;
	e2 <= 2;

	//numéro 4	
	#10ns
	opcode <= 6'h0c;
	e1 <= 3;
	immediate <= -2;

	//numéro 5
	#10ns
	format <= 1;  
	opcode <= 6'h04;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;
	
	//numéro 6
	#10ns
	opcode <= 6'h05;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;

	//numéro 7	
	#10ns
	opcode <= 6'h02;
	e1 <= 3;
	value <= 2;
	pc <= 100;

	//numéro 8
	#10ns
	opcode <= 6'h03;
	e1 <= 3;
	value <= 2;
	pc <= 100;

	//numéro 9
	#10ns
	opcode <= 6'h13;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	

	//numéro 10
	#10ns
	opcode <= 6'h12;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 11
	#10ns
	opcode <= 6'h0f;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 12
	#10ns
	opcode <= 6'h23;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	

	//numéro 13	
	#10ns
	opcode <= 6'h25;
	e1 <= 3;
	e2 <= 2;

	//numéro 14
	#10ns
	opcode <= 6'h0d;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 15	
	#10ns
	opcode <= 6'h28;
	e1 <= 3;
	e2 <= 2;

	//numéro 16
	#10ns
	opcode <= 6'h18;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;
		      		
	//numéro 17	
	#10ns
	opcode <= 6'h2c;
	e1 <= 3;
	e2 <= 2;

	//numéro 18
	#10ns
	opcode <= 6'h1c;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 19	
	#10ns
	format <= 0;
	opcode <= 6'h04;
	e1 <= 3;
	e2 <= 2;

	//numéro 20
	#10ns
	opcode <= 6'h14;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 21	
	#10ns
	opcode <= 6'h2a;
	e1 <= 3;
	e2 <= 2;

	//numéro 22
	#10ns
	opcode <= 6'h1a;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 23	
	#10ns
	opcode <= 6'h29;
	e1 <= 3;
	e2 <= 2;

	//numéro 24
	#10ns
	opcode <= 6'h19;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 25	
	#10ns
	opcode <= 6'h07;
	e1 <= 3;
	e2 <= 2;

	//numéro 26
	#10ns
	opcode <= 6'h17;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 27	
	#10ns
	opcode <= 6'h06;
	e1 <= 3;
	e2 <= 2;

	//numéro 28
	#10ns
	opcode <= 6'h16;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 29	
	#10ns
	opcode <= 6'h22;
	e1 <= 3;
	e2 <= 2;

	//numéro 30
	#10ns
	opcode <= 6'h0a;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	

	//numéro 31
	#10ns
	opcode <= 6'h2b;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
		
	//numéro 32	
	#10ns
	opcode <= 6'h26;
	e1 <= 3;
	e2 <= 2;

	//numéro 33
	#10ns
	opcode <= 6'h0e;
	e1 <= 3;
	immediate <= -2;
	pc <= 100;	
	
	
     end

endmodule // tb_reg